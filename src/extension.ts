export var powerProfileIndicatorExtensionInstance:any;
// @ts-ignore
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
// @ts-ignore
import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Log from './modules/log.js';
import * as Panel from './modules/panel.js';
import * as Resources from './modules/resources.js';

export default class PowerProfileIndicatorExtension extends Extension {
  PowerProfileIndicatorInstance: any = null;
  systemMenu: any = null;

  constructor(metadata: any) {
    super(metadata);
    // @ts-ignore
    Resources.File.extensionPath = this.path;
    powerProfileIndicatorExtensionInstance = this;
  }

  enable() {
    this.systemMenu = Main.panel.statusArea.quickSettings;

    if (!this.systemMenu) {
      Log.raw("init", "system menu is not defined");
      return false;
    }

    if (this.systemMenu._powerProfiles) {
      this.PowerProfileIndicatorInstance = new Panel.powerProfileIndicator();
    }
  }

  disable() {
    if (this.PowerProfileIndicatorInstance) {
      this.PowerProfileIndicatorInstance.stop();

      if (this.PowerProfileIndicatorInstance._indicator)
        this.PowerProfileIndicatorInstance._indicator.destroy();

      this.PowerProfileIndicatorInstance._indicator = null;

      this.PowerProfileIndicatorInstance.destroy();

      this.PowerProfileIndicatorInstance = null;
    }
  }

  public populate() {
      // removing the power item and adding it afterwards
      // any ability to change the positions of childs?
      if (this.PowerProfileIndicatorInstance !== null) {
        if (this.systemMenu._system)
          this.systemMenu._indicators.remove_child(this.systemMenu._system);
  
        this.systemMenu._indicators.add_child(this.PowerProfileIndicatorInstance);
  
        if (this.systemMenu._system)
          this.systemMenu._indicators.add_child(this.systemMenu._system);
      }
  }
}

declare const imports: any;

// @ts-ignore
import GLib from 'gi://GLib';
import * as Log from './log.js';

export class File {
  public static extensionPath: string;
  public static DBus(name: string) {
    let file = `${File.extensionPath}/resources/dbus/${name}.xml`;
    try {
      let [_ok, bytes] = GLib.file_get_contents(file);
      if (!_ok) Log.raw(`Couldn't read contents of "${file}"`);
      return _ok ? imports.byteArray.toString(bytes) : null;
    } catch (e) {
      Log.raw(`Failed to load "${file}"`, e);
    }
  }
}

// @ts-ignore
import Gio from 'gi://Gio';
// @ts-ignore
import GObject from 'gi://GObject';
// @ts-ignore
import * as quickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';
import * as Log from './log.js';
import * as Resources from './resources.js';
import * as Extension from '../extension.js'

const PROFILE_ICONS: {
  "performance": string;
  "balanced": string;
  "power-saver": string;
} = {
  "performance": "power-profile-performance-symbolic",
  "balanced": "power-profile-balanced-symbolic",
  "power-saver": "power-profile-power-saver-symbolic"
};

export const powerProfileIndicator = GObject.registerClass(
  class powerProfileIndicator extends quickSettings.SystemIndicator {
    powerProfilesProxy: any = null; // type: Gio.DbusProxy (donno how to add)
    powerProfilesSignalProxy: any = null; // type: Gio.DbusProxy (donno how to add)
    connected: boolean = false;
    _state: string = "balanced";
    profiles: any = [];

    _init() {
      super._init();
      this.createProxy();
    }

    async createProxy() {
      // @ts-ignore
      this._indicator = this._addIndicator();
      // @ts-ignore
      this._indicator.icon_name = "power-profile-balanced-symbolic";
      // @ts-ignore
      this._indicator.visible = true;

      let xmlProfiles = Resources.File.DBus("power-profiles-daemon-0.20-org.freedesktop.UPower.PowerProfiles");
      this.powerProfilesProxy = await new Gio.DBusProxy.makeProxyWrapper(xmlProfiles)(
        Gio.DBus.system,
        "org.freedesktop.UPower.PowerProfiles",
        "/org/freedesktop/UPower/PowerProfiles",
        // @ts-ignore
        (proxy, e) => {
          try {
            this.connected = true;
            if (this.updateProfile())
              Extension.powerProfileIndicatorExtensionInstance.populate();
          } catch (error) {
            Log.raw('could not get ActiveProfile', error);
          }
        }
      );

      let xmlSignals = Resources.File.DBus(
        "power-profiles-daemon-0.20-org.freedesktop.DBus.Properties"
      );
      this.powerProfilesSignalProxy = await new Gio.DBusProxy.makeProxyWrapper(
        xmlSignals
      )(
        Gio.DBus.system,
        "org.freedesktop.UPower.PowerProfiles",
        "/org/freedesktop/UPower/PowerProfiles",
        // @ts-ignore
        (proxy, e) => {
          try {
            proxy.connectSignal(
              "PropertiesChanged",
              // @ts-ignore
              (name: string = "", variant: any, profile: String[]) => {
                this.updateProfile();
              }
            );
          } catch (error) {
            Log.raw('error creating signal', error);
          }
        }
      );
    }

    updateProfile() {
      if (this.connected)
        try {
          this._state = this.powerProfilesProxy.ActiveProfile;

          if (typeof this._state !== 'string')
            return false;

          // @ts-ignore
          this._indicator.icon_name = PROFILE_ICONS[this._state];

          return true;
        } catch (error) {
          Log.raw("updateProfile", error);
        }

      return false;
    }

    stop() {
      this.connected = false;
      this.powerProfilesProxy = null;
      this.powerProfilesSignalProxy = null;
    }
  }
);

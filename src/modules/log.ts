export function raw(text: string = "", e: any = null) {
  if (e !== null)
    // @ts-ignore
    logError(e, `power-profile-indicator: ${text}`);
  else {
    // @ts-ignore
    log(`power-profile-indicator: ${text}`);
  }
}

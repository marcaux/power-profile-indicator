# power-profile-indicator

Extension for visualizing the current power profile in the status icon area of the top panel.

![screenshot](https://gitlab.com/marcaux/power-profile-indicator/-/raw/main/screenshot.png)
---

## Requirements

* gnome >= 45
* [Power Profiles Daemon](https://gitlab.freedesktop.org/hadess/power-profiles-daemon) >= 0.20

---

## Build Instructions

### Dependencies

* nodejs >= 14.0.0
* npm >= 6.14.0

### Building (production)

In a terminal enter the following commands as a user (**do NOT run as root or sudo**):

```bash
git clone https://gitlab.com/marcaux/power-profile-indicator.git /tmp/power-profile-indicator && cd /tmp/power-profile-indicator
npm install
npm run build && npm run install-user
```

_HINT: You will need to reload the GNOME Shell afterwards. (`Alt + F2` -> `r` on X11, `logout` on Wayland)_

### Building (development)

Instead of the
`npm run build && npm run install-user`
above, use this line instead:
`npm run build && npm run install-dev`

This will remove any production versions and installs the development version instead.

### Source debugging

`cd` into the directory where you've downloaded the `power-profile-indicator` source code and enter the following commands:

```bash
npm install
npm run debug
```

---

## License & Trademarks

**License:** Mozilla Public License Version 2.0 (MPL-2)

